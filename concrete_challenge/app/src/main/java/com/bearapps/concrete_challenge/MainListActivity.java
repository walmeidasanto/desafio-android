package com.bearapps.concrete_challenge;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainListActivity extends ListActivity {

    public static final String TAG = MainListActivity.class.getSimpleName();
    protected JSONObject mBlogData;
    protected ProgressBar mProgressBar;
    public MyAdapter adapter;
    private TextView title;

    private ListView list;
    private Button btn_prev;
    private Button btn_next;
    private int pageCount;
    private int increment = 1;
    private int TOTAL_LIST_ITEMS = 0;
    private int NUM_ITEMS_PAGE = 0;
    private boolean syncing = true;

    private final String KEY_TITLE = "title";
    private final String KEY_VIEWS = "views_count";
    private final String KEY_ID = "id";
    private final String KEY_DESCRIPTION = "description";
    private final String KEY_NAME = "name";
    private final String KEY_URLAVATAR = "avatar_url";
    private final String KEY_URL = "image_url";
    private final String BLOGURL = "http://api.dribbble.com/shots/popular?page=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        btn_prev = (Button) findViewById(R.id.prev);
        btn_next = (Button) findViewById(R.id.next);
        title = (TextView) findViewById(R.id.title);
        list = (ListView) getListView();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Intent i = new Intent(MainListActivity.this, ActivityEditor.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("title", adapter.getItem(position).title)
                        .putExtra("urlavatar", adapter.getItem(position).urlAvatar)
                        .putExtra("urlThumbnail", adapter.getItem(position).urlThumbnail)
                        .putExtra("countview", adapter.getItem(position).views)
                        .putExtra("description", adapter.getItem(position).description)
                        .putExtra("name", adapter.getItem(position).authName)  ;
                parent.getContext().startActivity(i);


            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment++;
                syncing = true;
                loadList(increment);
                btn_next.setEnabled(false);
                btn_prev.setEnabled(false);

                mProgressBar.setVisibility(View.VISIBLE);
                ArrayList<Posts> blogPosts = new ArrayList<Posts>();
                adapter = new MyAdapter(MainListActivity.this,
                        R.layout.list,
                        blogPosts);
                setListAdapter(adapter);

            }
        });

        btn_prev.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment--;
                loadList(increment);
                syncing = true;
                btn_prev.setEnabled(false);
                btn_next.setEnabled(false);

                mProgressBar.setVisibility(View.VISIBLE);
                ArrayList<Posts> blogPosts = new ArrayList<Posts>();
                adapter = new MyAdapter(MainListActivity.this,
                        R.layout.list,
                        blogPosts);

                setListAdapter(adapter);
            }
        });
        btn_next.setEnabled(false);
        btn_prev.setEnabled(false);
        if (isNetworkAvailable()) {
            mProgressBar.setVisibility(View.VISIBLE);
            loadList(1);
        } else {
            Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //CheckEnable();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }

    public void handleBlogResponse() {

        if (mBlogData == null) {
            updateDisplayForError();
        } else {
            try {

                if (TOTAL_LIST_ITEMS == 0) {
                    TOTAL_LIST_ITEMS = mBlogData.getInt("total");
                    NUM_ITEMS_PAGE = mBlogData.getInt("pages");
                    int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
                    val = val == 0 ? 0 : 1;
                    pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
                }
                JSONArray ArrayPosts = mBlogData.getJSONArray("shots");
                ArrayList<Posts> blogPosts = new ArrayList<Posts>();

                for (int i = 0; i < ArrayPosts.length(); i++) {
                    JSONObject JSONpost = ArrayPosts.getJSONObject(i);
                    JSONObject JSONplayer = JSONpost.getJSONObject("player");

                    String title = JSONpost.getString(KEY_TITLE);
                    title = Html.fromHtml(title).toString();

                    String description = JSONpost.getString(KEY_DESCRIPTION);
                    description = Html.fromHtml(description).toString();

                    String url = JSONpost.getString(KEY_URL);
                    url = Html.fromHtml(url).toString();
                    String views = JSONpost.getString(KEY_VIEWS);
                    views = Html.fromHtml(views).toString();
                    String id = JSONpost.getString(KEY_ID);
                    id = Html.fromHtml(id).toString();

                    String avatarurl = JSONplayer.getString(KEY_URLAVATAR);
                    avatarurl = Html.fromHtml(avatarurl).toString();
                    String name = JSONplayer.getString(KEY_NAME);
                    name = Html.fromHtml(name).toString();

                    Posts post = new Posts(title, url, views, id, avatarurl, name,description   );
                    blogPosts.add(post);
                }


                adapter = new MyAdapter(MainListActivity.this,
                        R.layout.list,
                        blogPosts);

                setListAdapter(adapter);
                title.setText("Page " + (increment) + " of " + pageCount);
                mProgressBar.setVisibility(View.GONE);

            } catch (JSONException e) {
                Log.e(TAG, getString(R.string.exception), e);
            }
        }
    }

    private void updateDisplayForError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.error_title));
        builder.setMessage(getString(R.string.error_message));
        builder.setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();

        TextView emptyTextView = (TextView) getListView().getEmptyView();
        emptyTextView.setText(getString(R.string.no_items));
    }

    private class GetBlogPostsTask extends AsyncTask<Object, Void, JSONObject> {
        private int page = 0;

        public GetBlogPostsTask(int page) {
            super();
            this.page = page;
        }

        @Override
        protected JSONObject doInBackground(Object... arg0) {
            int responseCode = -1;
            JSONObject jsonResponse = null;

            try {
                URL blogFeedUrl = new URL(BLOGURL + page);
                HttpURLConnection connection = (HttpURLConnection) blogFeedUrl.openConnection();
                connection.connect();

                responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = connection.getInputStream();
                    Reader reader = new InputStreamReader(inputStream);
                    int nextCharacter; // read() returns an int, we cast it to char later
                    String responseData = "";
                    while (true) { // Infinite loop, can only be stopped by a "break" statement
                        nextCharacter = reader.read(); // read() without parameters returns one character
                        if (nextCharacter == -1) // A return value of -1 means that we reached the end
                            break;
                        responseData += (char) nextCharacter; // The += operator appends the character to the end of the string
                    }

                    jsonResponse = new JSONObject(responseData);
                } else {
                    Log.i(TAG, getString(R.string.error_http_response) + responseCode);
                }
            } catch (MalformedURLException e) {
                Log.e(TAG, getString(R.string.exception), e);
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.exception), e);
            } catch (Exception e) {
                Log.e(TAG, getString(R.string.exception), e);
            }

            return jsonResponse;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            mBlogData = result;
            handleBlogResponse();
            syncing = false;
            CheckEnable();

        }

    }

    /**
     * Method for enabling and disabling Buttons
     */
    private void CheckEnable() {
        if (syncing) {
            btn_prev.setEnabled(false);
            btn_prev.setEnabled(false);
        } else if (increment == 1 && pageCount == 0) {
            btn_next.setEnabled(true);
            btn_prev.setEnabled(true);
        } else if (increment == pageCount) {
            btn_prev.setEnabled(true);
            btn_next.setEnabled(false);
        } else if ( increment == 1){
            btn_prev.setEnabled(false);
            btn_next.setEnabled(true);
        } else {
            btn_prev.setEnabled(true);
            btn_next.setEnabled(true);
        }
    }

    /**
     * Method for loading data in listview
     *
     * @param number
     */
    private void loadList(int number) {
        GetBlogPostsTask getBlogPostsTask = new GetBlogPostsTask(number);
        getBlogPostsTask.execute();
    }

}
