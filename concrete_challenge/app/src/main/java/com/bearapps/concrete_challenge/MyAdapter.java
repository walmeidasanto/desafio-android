package com.bearapps.concrete_challenge;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ursow on 18/07/15.
 */
public class MyAdapter extends ArrayAdapter<Posts> {

    private List<Posts> posts = null;
    Context context;
    int id;


    public MyAdapter(Context context,int id, List<Posts> posts) {
        super(context,id,posts);
        this.posts = posts;
        this.id = id;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        PostsHolder holder = null;

        if(convertView == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(id, parent, false);

            holder = new PostsHolder();
            holder.thumbnail = (ImageView)convertView.findViewById(R.id.thumbnail);
            holder.description = (TextView)convertView.findViewById(R.id.title);
            holder.views = (TextView)convertView.findViewById(R.id.countview);

            convertView.setTag(holder);
        }
        else
        {
            holder = (PostsHolder)convertView.getTag();
        }

        Posts post = posts.get(position);
        holder.description.setText(post.title);
        holder.views.setText(post.views);


        Picasso.with(this.context).load(post.urlThumbnail).into(holder.thumbnail);
        return convertView;
    }
    static class PostsHolder
    {
        ImageView thumbnail;
        TextView description;
        TextView views;
    }
}
