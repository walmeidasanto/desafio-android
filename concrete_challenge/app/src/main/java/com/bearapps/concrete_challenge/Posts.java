package com.bearapps.concrete_challenge;
/**
 * Created by ursow on 18/7/15.
 */
public class Posts {

    protected String title;
    protected String urlThumbnail;
    protected String views;
    protected String id;
    protected String urlAvatar;
    protected String authName;
    protected String description;

    public Posts(String title, String urlThumbnail, String views, String id,String urlAvatar, String authName, String descripition) {
        this.title = title;
        this.urlThumbnail = urlThumbnail;
        this.views = views;
        this.id = id;
        this.urlAvatar = urlAvatar;
        this.authName = authName;
        this.description = descripition;

    }


}

